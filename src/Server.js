const http = require('http');
const host = 'localhost';
const port = 8080;
const requestListener = function (req, res) {
    switch(req.url.split('/').length){
        case 2:
            if (req.url.split('/')[1] == "db"){
                res.writeHead(200)
                res.end("db");
            }
            break;
        case 3:
            if (req.url.split('/')[1] == "db"){
                let source = req.url.split('/')[2];
                res.writeHead(200);
                res.end("db");
            }
            break;
        case 4:
            break;
        default:
            res.writeHead(404);
            res.end("undefined");
            break;

    }
};
const server = http.createServer(requestListener);
server.listen(port, host, () => {
    console.log(`Server is running on http://${host}:${port}`);
});