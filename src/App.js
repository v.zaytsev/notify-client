import React, {useState} from 'react';
import Intro from "./Pages/Intro/Intro";
import Body from "./Pages/Body/Body";
import FoundPosts from "./Pages/FoundPosts/FoundPosts";
import {BrowserRouter, Routes, Route, Link, Router} from "react-router-dom";
import Header from "./Header";
import "./Styles/Header.css"

function App() {
    /*
    Getting posts
    const [posts, setPosts] = useState(some function getting data from the database);
     */

    const [sites, setSites] = useState([]);
    const [postsDisplayed, setPosts] = useState([]);
    const [loading, setLoading] = useState(1);
    const [bodyShownFor, setBodyShownFor] = useState("");


  return (
      <BrowserRouter>
          <div className="jsProject">
              <Header/>
              <Routes>
                  <Route exact path="/" element={<Intro sites={sites} setSites={setSites} setBodyShownFor={setBodyShownFor} />}/>
                  <Route exact path="/body" element={<Body sites={sites} setSites={setSites} setPosts={setPosts} setLoading={setLoading} bodyShownFor={bodyShownFor} />}/>
                  <Route exact path="/found_posts" element={<FoundPosts posts={postsDisplayed} loading={loading}/>}/>
                  <Route exact path="/about" element={<div>Поставьте 5 пж</div>}/>
                  <Route path="*" element={<div style={{fontSize: "20px", color: "white"}}>Site Not Found</div>}/>
              </Routes>
          </div>
      </BrowserRouter>
  );
}

export default App;
