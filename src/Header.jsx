import React from "react";
import { Link } from "react-router-dom";
import "./Styles/Header.css";

const Header = () => {
  return (
    <div className="header">
      <div style={{ display: "block", width:"33.333333333vw", justifyContent:"center"}}>
        <Link to="/" className="Link">
          Home
        </Link>
      </div>
      <div style={{ display: "block", width:"33.33333333333333vw"}}>
        <a
          href="https://gitlab.com/v.zaytsev/notify-app"
          className="Link"
        >
          Git
        </a>
      </div>
      <div style={{ display: "block", width:"33.33333333333333vw"}}>
        <Link to="/about" className="Link">
          About
        </Link>
      </div>
    </div>
  );
};

export default Header;
