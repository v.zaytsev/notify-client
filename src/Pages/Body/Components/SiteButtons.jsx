import React from 'react';
import './../Styles/Forms.css';
import {Link} from "react-router-dom";

const SiteButtons = (props) => {

    function getPosts() {
        props.setPosts([]);
        var xhr = new XMLHttpRequest();
        xhr.addEventListener('load', () => {
            let a = JSON.parse( xhr.responseText );
            if (a.length > 9)
                a.length = 9;
            props.setPosts(a);
            props.setLoading(0);
        });
        props.setLoading(1);
        let url = 'http://localhost:8000/database/' + props.site.md + '/' + props.site.id + '/news';
        // open the request with the verb and the url
        xhr.open('GET', url);
        // send the request
        xhr.send();
    }

    return (
        <div className="SiteButtons">
            <Link to="/found_posts"> <button className={"btnSiteButtons"} style={{backgroundColor: "rgb(240, 220, 70)"}} onClick={() => getPosts()}>Парсить</button> </Link>
            <button className={"btnSiteButtons"} style={{backgroundColor: "rgb(230, 60, 90)"}} onClick={() => props.remove(props.site)}>Удалить</button>
        </div>
    );
};

export default SiteButtons;