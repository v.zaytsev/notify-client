import React from 'react';
import FivedSites from "./FivedSites";

const SiteList = (props) => {

    function slc_5(sites) {
        const res = [];

        for (let i = 0; i < sites.length; i += 5) {
            res.push(sites.slice(i, i + 5));
        }

        return res;
    }

    const fivedSites = slc_5(props.sites);

    return (
        <div className={"SiteList"}>
            {fivedSites.map(arr =>
                <FivedSites remove={props.remove} sites={arr} setPosts={props.setPosts} setLoading={props.setLoading}/>
            )}
        </div>
    );
};

export default SiteList;