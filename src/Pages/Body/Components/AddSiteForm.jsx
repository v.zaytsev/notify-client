import React from "react";

const AddSiteForm = (props) => {
  return (
    <form
      style={{
        display: "flex",
        flexDirection: "column",
        margin: "0 0 10px 0",
        alignItems: "center",
      }}
    >
      <div style={{ display: "flex", flexDirection: "row" }}>
        <input
          className="bodyAddSiteInput"
          disabled={props.sitesLen >= 20}
          maxlength="32"
          style={{ width: "200px" }}
          onChange={(e) => props.setInptName(e.target.value)}
          value={props.inptName}
          placeholder={"Name"}
        />
        <input
          className="bodyAddSiteInput"
          disabled={props.sitesLen >= 20}
          style={{ width: "400px" }}
          onChange={(e) => props.setInptUrl(e.target.value)}
          value={props.inptUrl}
          placeholder={"URL"}
        />
      </div>
      <button
        className={"addSiteFormButton"}
        disabled={props.sitesLen >= 20}
        onClick={props.addSite}
      >
        Add site
      </button>
    </form>
  );
};

export default AddSiteForm;
