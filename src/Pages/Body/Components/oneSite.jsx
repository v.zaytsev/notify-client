import React from "react";
import "./../Styles/Body.css";
import SiteList from "./SiteList";
import SiteButtons from "./SiteButtons";

const OneSite = (props) => {
  return (
    <div className="oneSite">
        <p className="oneSiteP">{props.site.txt}</p>
        <SiteButtons remove={props.remove} site={props.site} setPosts={props.setPosts} setLoading={props.setLoading}/>
    </div>
  );
};

export default OneSite;
