import React from 'react';
import OneSite from "./oneSite";
import './../Styles/Body.css';

const FivedSites = (props) => {

    return (
        <div className="FivedSites">
            {props.sites.map(site =>
                <OneSite remove={props.remove} site={site} setPosts={props.setPosts} setLoading={props.setLoading}/>
            )}
        </div>
    );
};

export default FivedSites;