import React, { useState } from "react";
import {useLocation} from 'react-router-dom';
import "./Styles/Body.css";
import SiteList from "./Components/SiteList";
import AddSiteForm from "./Components/AddSiteForm";

const Body = (props) => {
  const location = useLocation();

  const aSite = { txt: "This", url: "https://google.com" };

  const [inptName, setInptName] = useState("");
  const [inptUrl, setInptUrl] = useState("");

  function addSite(e) {
    e.preventDefault();
    let newSite = { txt: inptName, url: inptUrl, md: props.bodyShownFor };
    props.setSites([...props.sites, newSite]);
    setInptName("");
    setInptUrl("");

    let json = JSON.stringify(newSite);
    let xhr = new XMLHttpRequest();
    xhr.open('POST', "http://localhost:8000/database");
    xhr.send(json);
  }

  function removeSite(site) {
    props.setSites(
      props.sites.filter((s) => !(s.url === site.url && s.txt === site.txt))
    );

    let json = JSON.stringify(site);
    let xhr = new XMLHttpRequest();
    xhr.open('POST', "http://localhost:8000/database");
    console.log(json);
    xhr.send(json);
  }

  return (
    <div className="Body">
      <link
        href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css"
        rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC"
        crossOrigin="anonymous"
      />
      <h1 className="Title">Choose a site</h1>
      <SiteList remove={removeSite} sites={props.sites} setPosts={props.setPosts} setLoading={props.setLoading}/>

      <AddSiteForm
        setInptName={setInptName}
        setInptUrl={setInptUrl}
        inptName={inptName}
        inptUrl={inptUrl}
        addSite={addSite}
        sitesLen={props.sites.length}
      />
    </div>
  );
};

export default Body;
