import React, {useState} from "react";
import { useNavigate } from "react-router-dom";
import Button from "./Components/Button";
import "./Styles/Intro.css";

const Intro = (props) => {
  const navigate = useNavigate();
  function getData(id) {
    // create a new XMLHttpRequest

    var xhr = new XMLHttpRequest()

    // get a callback when the server responds
    xhr.addEventListener('load', () => {
      // update the state of the component with the result here
      let a = JSON.parse(xhr.responseText);
      if (id === "vk") {
        navigate('/body', { state: "vk_groups" });
        props.setSites(a.filter((el) => !(el.url.search("vk.com") === -1)));
      }
      else if (id === "vc") {
        navigate('/body', { state: "vc_tapes" });
        props.setSites(a.filter((el) => !(el.url.search("vc.ru") === -1)));
      }
      else if (id === "rd") {
        navigate('/body', { state: "rd_tech" });
        props.setSites(a.filter((el) => !(el.url.search("reddit.com") === -1)));
      }
    })
    // open the request with the verb and the url
    xhr.open('GET', 'http://localhost:8000/database');
    // send the request
    xhr.send()
  }

  return (
    <>
    <div className="Intro" >
      <link
        href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css"
        rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC"
        crossOrigin="anonymous"
      />
      <h1 className="IntroTitle">Intro</h1>
      <div id="Button" style={{ display: "flex", flexDirection: "row" }}>
        <Button name="Vkontakte" onClick = {() => {getData("vk"); props.setBodyShownFor("vk_groups");}}/>
        <Button name="Vc" onClick = {() => {getData("vc"); props.setBodyShownFor("vc_tapes");}}/>
        <Button name="Reddit" onClick = {() => {getData("rd"); props.setBodyShownFor("r_tech");}}/>
      </div>
    </div>
    </>
  );
}

export default Intro;
