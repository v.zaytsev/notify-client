import React, { useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import "./../Styles/Intro.css";

const Button = ({children, ...props}) => {
  return (
    <>
      <Link to="/body" state={props.name}>
        <button
          className="IntroButton"
          type="button"
            {...props}
        >
            {props.name}
        </button>
      </Link>
    </>
  );
};

export default Button;
