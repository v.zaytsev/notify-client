import React from 'react';
import './../Styles/loader.css';

const Loader = () => {
    return (
        <div className='Loader'>
            <p className='LoaderP'>This is going to take</p>
            <p className='LoaderP'> a while...</p>
            <div className="lds-facebook">
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
            </div>
        </div>
    );
};

export default Loader;