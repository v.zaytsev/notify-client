import React from 'react';
import ANewPost from "./aNewPost";
import "../Styles/newPostsStyles.css";

const ThreePosts = (props) => {

    return (
        <div className="threePosts">
            {props.posts.map(post =>
                <ANewPost postText={post.txt} postTitle={post.title} url={post.url}/>
            )}
        </div>
    );
};

export default ThreePosts;