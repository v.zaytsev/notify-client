import React from 'react';
import "../Styles/newPostsStyles.css";
import ThreePosts from "./threePosts";

const PostList = (props) => {

    function slc_3(posts) {
        const res = [];

        for (let i = 0; i <posts.length; i += 3) {
            res.push(posts.slice(i, i + 3));
        }

        return res;
    }

    const threeedPosts = slc_3(props.posts);

    return (
        <div className="PostList3columns">
            {threeedPosts.map(posts3 =>
                <ThreePosts posts={posts3}/>
            )}
        </div>

    );
};

export default PostList;