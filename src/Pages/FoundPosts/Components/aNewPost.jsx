import React from 'react';
import "../Styles/newPostsStyles.css";

const ANewPost = (props) => {
    function leave(url) {
        window.open(url);
    }
    return (
        <div className="NewsPost">
            <h3 className="PostTitle">{props.postTitle}</h3>
            <span className="PostText">{props.postText}</span>
            <button className="btnLeaveSite" onClick={() => leave(props.url)}>Перейти</button>
        </div>
    );
};

export default ANewPost;