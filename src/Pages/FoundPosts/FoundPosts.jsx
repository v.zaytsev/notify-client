import React, { useState } from "react";
import PostList from "./Components/PostList";
import "./Styles/FoundPosts.css";
import "./Styles/newPostsStyles.css";
import Loader from "./Components/Loader";

const FoundPosts = (props) => {

  return (
    <div className="FoundPosts">
        <h1 className="FoundPostsTitle">Found posts</h1>
        {
            (props.loading)
                ? <Loader/>
                : <PostList posts={props.posts}/>
        }
      </div>
  );
};

export default FoundPosts;
